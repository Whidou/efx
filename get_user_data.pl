#!/usr/bin/perl
#
# get_user_data.pl: Parse Xooit HTML user data  dump
#
# This file is part of EFX.
#
# EFX is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Foobar is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# EFX.  If not, see <http://www.gnu.org/licenses/>.
#
# Author:
#   Whidou <whidou@openmailbox.org>

use strict;
use DBI;

my $db;
my $insert_statement;

$db = DBI->connect("dbi:SQLite:dbname=tmpdb.sqlite", "", "");
$db->{AutoCommit} = 0;

$db->do(<<'END_QUERY');
DROP TABLE IF EXISTS 'users'
END_QUERY

$db->do(<<'END_QUERY');
CREATE TABLE IF NOT EXISTS 'users' (
	'id' INTEGER PRIMARY KEY AUTOINCREMENT,
	'oldid' INTEGER NOT NULL,
	'name' TEXT NOT NULL DEFAULT '',
	'email' TEXT NOT NULL DEFAULT '',
	'regdate' INTEGER NOT NULL DEFAULT 0,
	'birthday' TEXT NOT NULL DEFAULT '',
	'website' TEXT NOT NULL DEFAULT '',
	'avatar' TEXT NOT NULL DEFAULT '',
	'signature' TEXT NOT NULL DEFAULT '')
END_QUERY

$db->do(<<'END_QUERY');
DELETE FROM 'users'
END_QUERY

$db->do(<<'END_QUERY');
UPDATE 'SQLITE_SEQUENCE' SET seq = 47 WHERE name = 'users'
END_QUERY

$insert_statement = $db->prepare(<<'END_QUERY');
INSERT INTO 'users' (oldid, name, email, birthday, website, avatar, signature)
VALUES (?, ?, ?, ?, ?, ?, ?)
END_QUERY

while (@ARGV > 0) {
	my $id;
	my $name;
	my $email;
	my $birthday = '';
	my $website = '';
	my $avatar = '';
	my $signature = '';

	$_ = shift();
	open(FILE, $_) || die $!;

	s/^.*u=(\d+)&.*$/\1/;
	$id = $_;

	while (<FILE>) {
		chomp();
		if (/^<input .*id="rand\d+\[username\].*value="(.*?)"/) {
			$name = $1;
		} elsif (/^<input .*id="rand\d+\[email\].*value="(.*?)"/) {
			$email = lc($1);
		} elsif (/<label[ ]for="Anniversaire.*
			  selected">(\d{2})<.*
			  selected">(\d{2})[ ].*
			  value="(\d{4})"/x) {
			$birthday = "$1-$2-$3";
		} elsif (/^<input .*id="website".*value="(.*?)"/) {
			$website = lc($1);
		} elsif (/^<dd><img src="http:\/\/(.*?)" /) {
			$avatar = $1;
		}
	}

	seek(FILE, 0, 0);
	local $/ = undef;
	$_ = <FILE>;
	if (/<textarea[^>]*?id="signature[^>]*?>\n*((.|\s)*?)\n*<\/textarea>/) {
		$signature = $1;
	}

	close(FILE);

	$insert_statement->execute($id, $name, $email, $birthday, $website,
				  $avatar, $signature) if ($name and $email);
}

$db->commit();
$db->disconnect();
