#!/usr/bin/perl
#
# get_structure.pl: Parse Xooit HTML dump for forum structure
#
# This file is part of EFX.
#
# EFX is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Foobar is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# EFX.  If not, see <http://www.gnu.org/licenses/>.
#
# Author:
#   Whidou <whidou@openmailbox.org>

use strict;
use DBI;

my $db;
my $forum_statement;
my $description_statement;
my $pos_statement;
my $update_parent_statement;
my $update_description_statement;
my $update_position_statement;
my $update_type_statement;
my $select_tree_statement;
my $select_parent_right_statement;
my $update_leftright_statement;
my $increment_right_statement;
my $increment_left_statement;
my $select_statement;

$db = DBI->connect("dbi:SQLite:dbname=tmpdb.sqlite", "", "");
$db->{AutoCommit} = 0;

$db->do(<<'END_QUERY');
DROP TABLE IF EXISTS 'forum'
END_QUERY

$db->do(<<'END_QUERY');
CREATE TABLE IF NOT EXISTS 'forum' (
	'id' INTEGER PRIMARY KEY AUTOINCREMENT,
	'oldid' INTEGER NOT NULL,
	'oldparent' INTEGER NOT NULL DEFAULT '0',
	'parent' INTEGER NOT NULL DEFAULT '0' REFERENCES forum,
	'title' TEXT NOT NULL DEFAULT '',
	'type' INTEGER NOT NULL DEFAULT '1')
END_QUERY

$db->do(<<'END_QUERY');
DELETE FROM 'forum'
END_QUERY

$db->do(<<'END_QUERY');
INSERT INTO 'forum' VALUES(0, 0, 0, 0, '', 0);
END_QUERY

$db->do(<<'END_QUERY');
UPDATE 'SQLITE_SEQUENCE' SET seq = 0 WHERE name = 'forum'
END_QUERY

$forum_statement = $db->prepare(<<'END_QUERY');
INSERT INTO forum (oldid,oldparent,title) VALUES (?,?,?)
END_QUERY

$db->do(<<'END_QUERY');
DROP TABLE IF EXISTS 'description'
END_QUERY

$db->do(<<'END_QUERY');
CREATE TABLE IF NOT EXISTS 'description' (
	'id' INTEGER PRIMARY KEY AUTOINCREMENT,
	'oldid' INTEGER NOT NULL,
	'newid' INTEGER NOT NULL DEFAULT '0' REFERENCES forum,
	'description' TEXT NOT NULL DEFAULT '')
END_QUERY

$db->do(<<'END_QUERY');
DELETE FROM 'description'
END_QUERY

$db->do(<<'END_QUERY');
UPDATE 'SQLITE_SEQUENCE' SET seq = 0 WHERE name = 'description'
END_QUERY

$description_statement = $db->prepare(<<'END_QUERY');
INSERT INTO description (oldid,description) VALUES (?,?)
END_QUERY

$db->do(<<'END_QUERY');
DROP TABLE IF EXISTS 'position'
END_QUERY

$db->do(<<'END_QUERY');
CREATE TABLE IF NOT EXISTS 'position' (
	'id' INTEGER PRIMARY KEY AUTOINCREMENT,
	'oldid' INTEGER NOT NULL,
	'newid' INTEGER NOT NULL DEFAULT '0' REFERENCES forum,
	'pos' INTEGER NOT NULL,
	'left' INTEGER NOT NULL DEFAULT '0',
	'right' INTEGER NOT NULL DEFAULT '0')
END_QUERY

$db->do(<<'END_QUERY');
DELETE FROM 'position'
END_QUERY

$db->do(<<'END_QUERY');
INSERT INTO 'position' (id, oldid, newid, pos, left, right)
VALUES ('0', '0', '0', '0', '0', '1')
END_QUERY

$db->do(<<'END_QUERY');
UPDATE 'SQLITE_SEQUENCE' SET seq = 0 WHERE name = 'position'
END_QUERY

$pos_statement = $db->prepare(<<'END_QUERY');
INSERT INTO position (oldid,pos) VALUES (?,?)
END_QUERY

$update_parent_statement = $db->prepare(<<'END_QUERY');
UPDATE 'forum'
SET parent = (
	SELECT id FROM 'forum' as 'parent'
	WHERE forum.oldparent = parent.oldid
)
END_QUERY

$update_description_statement = $db->prepare(<<'END_QUERY');
UPDATE 'description'
SET newid = (
	SELECT id FROM 'forum'
	WHERE description.oldid = forum.oldid
)
END_QUERY

$update_position_statement = $db->prepare(<<'END_QUERY');
UPDATE 'position'
SET newid = (
	SELECT id FROM 'forum'
	WHERE position.oldid = forum.oldid
)
END_QUERY

$update_type_statement = $db->prepare(<<'END_QUERY');
UPDATE 'forum' SET type = '0' WHERE id IN (SELECT parent FROM forum)
END_QUERY

$select_tree_statement = $db->prepare(<<'END_QUERY');
WITH RECURSIVE tree(id, pos) AS (
	VALUES('0', '0')
	UNION
	SELECT forum.id, position.pos
	FROM forum, tree, position
	WHERE (
		forum.parent=tree.id
		AND forum.id=position.newid
	)
	ORDER BY position.pos ASC
)
SELECT forum.id
FROM tree, forum
WHERE (
	forum.id = tree.id
	AND forum.id != 0
)
END_QUERY

$select_parent_right_statement = $db->prepare(<<'END_QUERY');
SELECT position.right
FROM 'position', 'forum' as 'child'
WHERE (
	child.id = ?
	AND position.newid = child.parent
)
END_QUERY

$update_leftright_statement = $db->prepare(<<'END_QUERY');
UPDATE 'position' SET left = ?, right = ? WHERE newid = ?
END_QUERY

$increment_right_statement = $db->prepare(<<'END_QUERY');
UPDATE 'position' SET right = right + 2 WHERE right >= ?
END_QUERY

$increment_left_statement = $db->prepare(<<'END_QUERY');
UPDATE 'position' SET left = left + 2 WHERE left >= ?
END_QUERY

#~ $select_statement = $db->prepare(<<'END_QUERY');
#~ SELECT forum.id, forum.parent, position.left, position.right, forum.title,
	#~ description.description, forum.type
	#~ FROM 'forum', 'description', 'position'
	#~ WHERE (
		#~ forum.id = description.newid
		#~ AND forum.id = position.newid
	#~ )
#~ UNION
#~ SELECT forum.id, forum.parent, position.left, position.right, forum.title, '',
	#~ forum.type
	#~ FROM 'forum', 'position'
	#~ WHERE (
		#~ forum.id NOT IN (
			#~ SELECT newid FROM 'description'
		#~ )
		#~ AND forum.id = position.newid
		#~ AND forum.id != 0
	#~ )
#~ ORDER BY forum.id
#~ END_QUERY

while (@ARGV > 0) {
	my $id;
	my $title;
	my $parent = 0;
	my $text;
	my $pos = 0;

	$_ = shift();
	open(FILE, $_) || die $!;

	if (/index\.html?$/) {
		while (<FILE>) {
			chomp();
			if (/<span class="cattitle"><a href="f(\d+)-/) {
				$pos_statement->execute($1, $pos);
				$pos++;
			}
		}
		next;
	}

	s/^.*f(\d+)-[^\/]*$/\1/;
	$id = $_;

	while (<FILE>) {
		chomp();
		if (/^<title>/) {
			s/^.*:: //;
			s/<\/title>$//;
			$title = $_;
		} elsif (/^-> /) {
			s/^.*f(\d+)-.*$/\1/;
			if ($_ != $id) {
				$parent = $_;
			}
		}
	}

	seek(FILE, 0, 0);
	local $/ = undef;
	$_ = <FILE>;
	while (/<a href="f(\d+)-.*?<br\s?\/>\s+(.*?)\s+<br\s?\/>/g) {
		$pos_statement->execute($1, $pos);
		$pos++;
		$description_statement->execute($1, $2) unless $2 =~ /^$/;
	}

	$forum_statement->execute($id, $parent, $title);

	close(FILE);
}

$db->commit();

$update_parent_statement->execute();
$update_description_statement->execute();
$update_position_statement->execute();
$db->commit();

$select_tree_statement->execute();

while (my @forum_row = $select_tree_statement->fetchrow_array()) {
	my @position_row;
	my $id;
	my $left;

	$id = $forum_row[0];

	$select_parent_right_statement->execute($id);
	@position_row = $select_parent_right_statement->fetchrow_array();
	@position_row = @position_row[0];
	$left = $position_row[0];
	$select_parent_right_statement->finish();

	$increment_right_statement->execute($left);
	$increment_left_statement->execute($left);
	$update_leftright_statement->execute($left, $left + 1, $id);
}

$update_type_statement->execute();
$db->commit();

#~ $select_statement->execute();

#~ while (my @row = $select_statement->fetchrow_array) {
	#~ $row[4] =~ s/'/''/g;
	#~ $row[5] =~ s/'/''/g;
	#~ print("($row[0], $row[1], $row[2], $row[3], '', '$row[4]', '$row[5]', ",
#~ "'', 7, '', '', '', 0, '', '', '', '', 7, '', 0, $row[6], 0, 0, 0, '', 0, '', ",
#~ "'', 48, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1, 0),\n");
#~ }

$db->disconnect();
