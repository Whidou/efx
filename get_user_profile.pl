#!/usr/bin/perl
#
# get_profile.pl: Parse Xooit HTML user profile dump
#
# This file is part of EFX.
#
# EFX is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Foobar is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# EFX.  If not, see <http://www.gnu.org/licenses/>.
#
# Author:
#   Whidou <whidou@openmailbox.org>

use strict;
use DBI;
use Time::Piece;

my $db;
my $regdate_statement;

$db = DBI->connect("dbi:SQLite:dbname=tmpdb.sqlite", "", "");
$db->{AutoCommit} = 0;

$regdate_statement = $db->prepare(<<'END_QUERY');
UPDATE 'users' SET regdate = ? WHERE oldid = ?
END_QUERY

my %months = (
	"Jan" => "01",
	"Fév" => "02",
	"Mar" => "03",
	"Avr" => "04",
	"Mai" => "05",
	"Juin" => "06",
	"Juil" => "07",
	"Aoû" => "08",
	"Sep" => "09",
	"Oct" => "10",
	"Nov" => "11",
	"Déc" => "12",
);

while (@ARGV > 0) {
	my $id;
	my $regdate;

	$_ = shift();
	open(FILE, $_) || die $!;

	s/^.*u=(\d+)&.*$/\1/;
	$id = $_;

	while (<FILE>) {
		chomp();
		if (/^<td.*?>(\d+) (.+?) (20\d+)<\/span><\/b><\/td>$/) {
			$regdate = Time::Piece->strptime("$3-$months{$2}-$1",
							 "%Y-%m-%d");
			last;
		}
	}

	close(FILE);

	$regdate_statement->execute($regdate->epoch, $id) if ($regdate);
}

$db->commit();
$db->disconnect();
